/**********************************************************************

MAIN

**********************************************************************/

require('gsap');
require('./components/dolly');

import inertia from 'wheel-inertia'
import raf from 'raf'
import mouseW from 'mouse-wheel'

export default class AppView {
	constructor(timelineItems) {

		this.timelineItems = timelineItems

		this.WIDTH 	= window.innerWidth
		this.HEIGHT = window.innerHeight
		this.FOV 	= 40
		this.R 		= 0
		this.target = 0

		this.taxiAnimated = false

		this.controller = { animate: false }

		this.points = {
			"camera": [
				{ "x": 0, "y": 20, "z": 20 },
				{ "x": 10, "y": 20, "z": 20 },
				{ "x": 20, "y": 20, "z": 20 },
				{ "x": 50, "y": 20, "z": 20 },
				{ "x": 60, "y": 20, "z": 11 },
				{ "x": 100, "y": 20, "z": 11 },
				{ "x": 200, "y": 20, "z": 11 },
				{ "x": 210, "y": 20, "z": 20 },
				{ "x": 360, "y": 20, "z": 20 },
				{ "x": 365, "y": 20, "z": 15 },
				{ "x": 385, "y": 20, "z": 15 },
				{ "x": 400, "y": 20, "z": 15 }
			],
			"lookat": [
				{ "x": 0, "y": 20, "z": -100 },
				{ "x": 10, "y": 20, "z": -100 },
				{ "x": 20, "y": 20, "z": -100 },
				{ "x": 50, "y": 20, "z": -100 },
				{ "x": 60, "y": 20, "z": -100 },
				{ "x": 100, "y": 20, "z": -100 },
				{ "x": 200, "y": 20, "z": -100 },
				{ "x": 210, "y": 20, "z": -100 },
				{ "x": 360, "y": 20, "z": -100 },
				{ "x": 365, "y": 25, "z": -100 },
				{ "x": 385, "y": 25, "z": -100 },
				{ "x": 400, "y": 25, "z": -100 }
			]
		};

		this.canvas   = document.getElementById('canvas');
		this.renderer = new THREE.WebGLRenderer({ antialias: true })
		this.renderer.setPixelRatio( window.devicePixelRatio );
		this.renderer.shadowMap.enabled = true
		this.renderer.shadowMap.type = THREE.PCFSoftShadowMap
		this.renderer.shadowMap.bias = 0.003;
		// this.renderer.shadowMap.renderSingleSided = false

		this.frustum = new THREE.Frustum()
		this.cameraViewProjectionMatrix = new THREE.Matrix4()

		this.camera  = new THREE.PerspectiveCamera( this.FOV, this.WIDTH / this.HEIGHT, 1, 10000 )
		this.cameraDolly = new THREE.PerspectiveCamera( this.FOV, this.WIDTH / this.HEIGHT, 1, 10000 )
		this.scene 	= new THREE.Scene()

		this.onWindowResize = this.onWindowResize.bind(this)
		this.mouseWheel = this.mouseWheel.bind(this)
		this.update = this.update.bind(this)
		this.render = this.render.bind(this)
		this.onMouseMove = this.onMouseMove.bind(this)

		this.texLoader = new THREE.TextureLoader()

		// DOLLY SETUP
		this.dolly = new THREE.CameraDolly(this.cameraDolly, this.scene, this.points)
		this.clock = new THREE.Clock( true )

		this.screen_range_x = Math.tan(this.cameraDolly.fov * Math.PI / 180 * 0.5) * this.cameraDolly.position.z * 2;
		console.log('this is', this.scene)

		// this.dolly.cameraPosition = 0.82;
		// this.dolly.lookatPosition = 0.82;
		// this.dolly.update();

		// Add renderer to DOM
		document.body.appendChild(this.renderer.domElement)

		this.addLights()
		this.addSceneImages()
		this.addVideos()
		this.addHelpers()
		this.addControls()
		this.addEventListeners()

		this.onWindowResize();
		this.update();
	}
	addLights() {
		// Add Light
		this.light = new THREE.DirectionalLight( 0xffffff, 1 );
		this.light.position.set(2, 0, 10)
		this.light.castShadow = true;
		this.light.shadow.mapSize.width = 2048;
		this.light.shadow.mapSize.height = 2048;
		// this.light.shadow.camera.near = 10;
		// this.light.shadow.camera.far = 100

		this.scene.add( this.light );

		var spotLightHelper = new THREE.DirectionalLightHelper( this.light );
		this.scene.add( spotLightHelper );
		spotLightHelper.update();

		// this.light = new THREE.AmbientLight( 0xFFFFFF); // soft white light
		// this.light.position.set(0, 10, -20)
		// this.scene.add( this.light )
	}
	addSceneImages() {
		// let texloader = new THREE.TextureLoader()
  		this.timelineItems.forEach((el) => {

			if (el.type === 'video') {
				this.addVideos(el)
				return
			}

			// let texLoader = new THREE.TextureLoader()
			let tex = this.texLoader.load(el.src, (tx) => {
				// tex.needsUpdate = true
				tex.minFilter = THREE.LinearFilter;
				let material = new THREE.MeshLambertMaterial({
					transparent: el.transparent === false ? false : true,
					map: tex
				})
				let plane = new THREE.Mesh(new THREE.PlaneGeometry(tex.image.width / 2, tex.image.height / 2), material)
				plane.receiveShadow = true
				plane.castShadow = true
				plane.position.set(el.position.x, el.position.y, el.position.z)
				plane.scale.x = el.scale
				plane.scale.y = el.scale
				plane.name = el.name
				this.scene.add(plane)
			})
		})
	}
	addVideos(el) {
		if (el) {
			console.log('video')
			var video = document.getElementById(el.name);
			video.src = el.src;
			video.load();
			video.play();
			var texture = new THREE.VideoTexture(video);
			texture.needsUpdate;
			texture.minFilter = THREE.LinearFilter;
			texture.magFilter = THREE.LinearFilter;
			texture.format = THREE.RGBFormat;
			texture.crossOrigin = 'anonymous';

			var imageObject = new THREE.Mesh(
				new THREE.PlaneGeometry(el.width, el.height),
				new THREE.MeshBasicMaterial({ map: texture }),);

			imageObject.position.set(el.position.x, el.position.y, el.position.z)
			imageObject.scale.x = el.scale
			imageObject.scale.y = el.scale

			this.scene.add( imageObject );
		}
	}
	addHelpers() {
		// Helpers
		// this.scene.add( new THREE.GridHelper(1000, 10))
		// this.scene.add( new THREE.AxesHelper(10))
		// this.scene.add( new THREE.CameraHelper(this.cameraDolly))
	}
	addControls() {
		// this.controls = new THREE.TrackballControls( this.camera, this.renderer.domElement );
		// this.controls.rotateSpeed          	= 1.0
		// this.controls.zoomSpeed            	= 1.2
		// this.controls.panSpeed             	= 0.8
		// this.controls.noZoom 	         	= false
		// this.controls.noPan  	          	= false
		// this.controls.staticMoving 		  	= true
		// this.controls.dynamicDampingFactor 	= 0
		//
		// var zoom = 200;
		// this.camera.position.set(1 * zoom, 0.75 * zoom, 1 * zoom);
		// this.camera.lookAt(new THREE.Vector3());
	}
	addEventListeners() {
		window.addEventListener('resize', this.onWindowResize, false);
		// window.addEventListener('mousemove', this.onMouseMove, false);
		inertia.addCallback(this.onScroll)
		mouseW(this.mouseWheel)
	}
	onMouseMove(e) {
		console.log('e is', e.movementX)
	}
	onScroll(direction) {
		// Direction on Scroll
		// console.log('on scroll', direction)
	}
	mouseWheel(dx, dy) {
		if (this.R < 0) {
			this.R = 0
			return
		}
		if (this.R > 1) {
			this.R = 1
			return
		}

		inertia.update(dy)
		this.R += dy / 30000
		let clamped = this.clamp(0, 1, this.R);

		this.dolly.cameraPosition = clamped;
		this.dolly.lookatPosition = clamped;
		this.dolly.update();

		console.log('this dolly camera position', this.dolly)
	}
	update() {
		requestAnimationFrame(this.update);

		// this.render( this.camera,  0, 0, 1, 1 );
		this.render( this.cameraDolly, 0, 0, 1, 1 );

		// this.light.position.copy( this.cameraDolly.position );
		// this.light.target.updateMatrixWorld();

		this.cameraDolly.updateMatrix();
		this.cameraDolly.updateMatrixWorld();

		var frustum = new THREE.Frustum();
		frustum.setFromMatrix(new THREE.Matrix4().multiplyMatrices(this.cameraDolly.projectionMatrix, this.cameraDolly.matrixWorldInverse));

		// Your 3d point to check
		var taxiPos = new THREE.Vector3(150, 15.3, -10);
		if (frustum.containsPoint(taxiPos)) this.animateTaxi()

		var tie1 = new THREE.Vector3(203, 26, -6);
		if (frustum.containsPoint(tie1)) this.animateTie()

		var delta = this.clock.getElapsedTime() * 0.02;
		var position = THREE.Math.mapLinear(Math.sin(delta), 0, 1, 0, 1);

		if(this.controller.animate){
			this.dolly.cameraPosition = position;
			this.dolly.lookatPosition = position;
			this.dolly.update();
		}

		// this.controls.update();
	}
	animateTaxi() {
		if (this.taxiAnimated) return
		this.taxiAnimated = true
		let object = this.scene.getObjectByName( 'ch-1-ny-ground-taxi', true );
		TweenMax.to(object.position, 6, {
			x: object.position.x + 24,
			ease: Expo.easeInOut,
			onComplete: () => console.log('done')
		})
	}
	animateTie() {
		if (this.tie1Animated) return
		this.tie1Animated = true
		let object = this.scene.getObjectByName( 'ch-2-ties-1', true );
		TweenMax.to(object.position, 2, {
			y: 26,
			ease: Bounce.easeOut,
			onComplete: () => console.log('tie done')
		})
	}
	render(camera, left, bottom, width, height) {
		left   *= this.WIDTH;
		bottom *= this.HEIGHT;
		width  *= this.WIDTH;
		height *= this.HEIGHT;
		//
		this.renderer.setViewport( left, bottom, width, height );
		this.renderer.setScissor( left, bottom, width, height );
		this.renderer.setScissorTest( true );
		//
		this.renderer.render( this.scene, camera );
	}

	onMouseWheel(e) {
		e.preventDefault();
		var delta = e.wheelDelta
		inertia.update(delta)
	}
	clamp(min, max, v) {
		if (v < min) {
	        return min;
	    } else if (v > max) {
	        return max;
	    } else {
	        return v;
	    }
	}
	onWindowResize() {
		this.WIDTH  = window.innerWidth;
		this.HEIGHT = window.innerHeight;

		// this.camera.aspect = this.WIDTH / this.HEIGHT;
		// this.camera.updateProjectionMatrix();

		this.cameraDolly.aspect = this.WIDTH / this.HEIGHT;
		this.cameraDolly.updateProjectionMatrix();

		this.renderer.setSize( this.WIDTH, this.HEIGHT );
	}
}
