/**********************************************************************

MAIN

**********************************************************************/


import axios from 'axios'
import ready from 'domready';
import AppView from './main'

ready(() => {
	window.app = new App();
});


export default class App {

	constructor() {
		this.initLoader();
	}

	initLoader() {
		axios.get(`./assets/datas/timeline.json`)
			.then((object) => {
				this.items = object.data.items
				this.initView()
			})
	}
	initView() {
		this.view = new AppView(this.items)
	}
}
